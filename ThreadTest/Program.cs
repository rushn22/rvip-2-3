﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading; //Именно это пространство имен поддерживает многопоточность
using System.Net;
using System.Net.Sockets;


namespace ThreadTest
{
    class Program
    {
        public static class Server
        {
            public static void Processing(string s)
            {
                Console.WriteLine(s + " -> " + ReqProc());
                Console.WriteLine(s + " -> " + DbConnect());
                Console.WriteLine(s + " -> " + RequestList());
                Console.WriteLine(s + " -> " + DbRequest());
                Console.WriteLine(s + " -> " + CreateResponce());
                Console.WriteLine(s + " -> " + GetAnswer());
            }
            public static string ReqProc()
            {
                string s = "Обработка запросов";
                Thread.Sleep(10);
                return s;
            }
            public static string DbConnect()
            {
                string db_connect = "Подключение к БД"; 
                Thread.Sleep(5);
                return db_connect;
            }
            public static string RequestList()
            {
                string req_list = "Формирование очереди запросов";
                Thread.Sleep(6);
                return req_list;
            }
            public static string DbRequest()
            {
                string db_req = "Обращение к БД";
                Thread.Sleep(7);
                return db_req;
            }
            public static string CreateResponce()
            {
                string resp = "Формирование ответа";
                Thread.Sleep(2);
                return resp;
            }
            public static string GetAnswer()
            {
                string answer = "Вывод результата";
                Thread.Sleep(8);
                return answer;
            }

        }

        static void Main(string[] args)
        {
            // Устанавливаем для сокета локальную конечную точку
            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);

            // Создаем сокет Tcp/Ip
            Socket sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Назначаем сокет локальной конечной точке и слушаем входящие сокеты
            try
            {
                sListener.Bind(ipEndPoint);
                sListener.Listen(10);

                // Начинаем слушать соединения
                while (true)
                {
                    Console.WriteLine("Ожидаем соединение через порт {0}", ipEndPoint);

                    // Программа приостанавливается, ожидая входящее соединение
                    Socket handler = sListener.Accept();
                    string data = null;

                    // Мы дождались клиента, пытающегося с нами соединиться

                    byte[] bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);

                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);

                    // Показываем данные на консоли
                    Console.Write("Полученный запрос: " + data + "\n\n");

                    Server.Processing(data);

                    // Отправляем ответ клиенту\
                    string reply = "Возврат результата для: " + data;
                    byte[] msg = Encoding.UTF8.GetBytes(reply);
                    handler.Send(msg);

                    if (data.IndexOf("<TheEnd>") > -1)
                    {
                        Console.WriteLine("Сервер завершил соединение с клиентом.");
                        break;
                    }

                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
       
            Console.Read(); //Приостановим основной поток
        }

    }
}
